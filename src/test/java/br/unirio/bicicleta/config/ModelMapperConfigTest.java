package br.unirio.bicicleta.config;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

class ModelMapperConfigTest {
	
	@Test
	void testModelMapper() {
		ModelMapperConfig modelMapperConfig = new ModelMapperConfig();
				
		assertTrue(modelMapperConfig.modelMapper() instanceof ModelMapper);
	}

}
