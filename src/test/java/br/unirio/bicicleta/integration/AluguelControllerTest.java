package br.unirio.bicicleta.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.error.ErrorResponse;
import br.unirio.bicicleta.model.aluguel.Aluguel;
import br.unirio.bicicleta.model.aluguel.Devolucao;
import br.unirio.bicicleta.model.dto.AluguelDTO;
import br.unirio.bicicleta.model.dto.DevolucaoDTO;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AluguelControllerTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@LocalServerPort
	private int port;
	
	@Test
	void realizarAluguel_TrancaNaoLocalizada_DeveLancarExcecao() throws UnirestException {
		
		AluguelDTO aluguelDTO = new AluguelDTO();
		aluguelDTO.setTrancaInicio(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cc7"));
		aluguelDTO.setCiclista(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab510"));
		
		ResponseEntity<ErrorResponse> response = 
				this.restTemplate.postForEntity("http://localhost:" + port + "/aluguel", aluguelDTO, ErrorResponse.class);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals("BAD400", response.getBody().getCodigo());
		assertEquals("Não foi possível encontrar a entidade solicitada", response.getBody().getMensagem());
		assertEquals("b9a029d6-8ba1-4be7-9dcb-2468baeb4cc7", response.getBody().getId());
	}
	
	@Test
	void realizarAluguel_DeveRegistrarAluguel() throws UnirestException, JsonProcessingException {
		
		AluguelDTO aluguelDTO = new AluguelDTO();
		aluguelDTO.setTrancaInicio(UUID.fromString("0d3ed1c2-f6fe-49be-bb4c-cbb0d5ce4663"));
		aluguelDTO.setCiclista(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab510"));
		
		ResponseEntity<Aluguel> response = 
				this.restTemplate.postForEntity("http://localhost:" + port + "/aluguel", aluguelDTO, Aluguel.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab510"), response.getBody().getCiclista());
		assertEquals(UUID.fromString("0d3ed1c2-f6fe-49be-bb4c-cbb0d5ce4663"), response.getBody().getTrancaInicio());
	}
	
	@Test
	void realizarAluguel_CiclistaPossuiAluguelEmAndamento_DeveLancarExcecao() throws UnirestException {
		
		AluguelDTO aluguelDTO = new AluguelDTO();
		aluguelDTO.setTrancaInicio(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cb6"));
		aluguelDTO.setCiclista(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab509"));
		
		ResponseEntity<ErrorResponse> response = 
				this.restTemplate.postForEntity("http://localhost:" + port + "/aluguel", aluguelDTO, ErrorResponse.class);
		
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
		assertEquals("BAD403", response.getBody().getCodigo());
		assertEquals("O ciclista já possui um aluguel em andamento", response.getBody().getMensagem());
		assertEquals("00a66fd2-873f-4e71-a1d4-8d3d717ab509", response.getBody().getId());
	}
	
	@Test
	void realizarAluguel_CiclistaNaoLocalizado_DeveLancarExcecao() throws UnirestException {
		
		AluguelDTO aluguelDTO = new AluguelDTO();
		aluguelDTO.setTrancaInicio(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cb6"));
		aluguelDTO.setCiclista(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab512"));

		ResponseEntity<ErrorResponse> response =
				this.restTemplate.postForEntity("http://localhost:" + port + "/aluguel", aluguelDTO, ErrorResponse.class);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals("BAD400", response.getBody().getCodigo());
		assertEquals("Não foi possível encontrar a entidade solicitada", response.getBody().getMensagem());
		assertEquals("00a66fd2-873f-4e71-a1d4-8d3d717ab512", response.getBody().getId());
	}
	
	@Test
	void realizarAluguel_CiclistaCadastroNaoAtivo_DeveLancarExcecao() throws UnirestException {
		
		AluguelDTO aluguelDTO = new AluguelDTO();
		aluguelDTO.setTrancaInicio(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cb6"));
		aluguelDTO.setCiclista(UUID.fromString("00a66fd2-873f-4e71-a1d4-8d3d717ab511"));
		
		ResponseEntity<ErrorResponse> response =
				this.restTemplate.postForEntity("http://localhost:" + port + "/aluguel", aluguelDTO, ErrorResponse.class);
		
		assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
		assertEquals("CIC401", response.getBody().getCodigo());
		assertEquals("O ciclista informado não possui cadastro ativo", response.getBody().getMensagem());
		assertEquals("00a66fd2-873f-4e71-a1d4-8d3d717ab511", response.getBody().getId());
	}
	
	@Test
	void realizarDevolucao_DeveRegistrarDevolucao() throws UnirestException, JsonProcessingException {
		
		DevolucaoDTO devolucaoDTO = new DevolucaoDTO();
		devolucaoDTO.setIdBicicleta(UUID.fromString("82cfcc93-dafc-4b5f-8ec1-5082fe57a4bd"));
		devolucaoDTO.setIdTranca(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cb6"));
		
		ResponseEntity<Devolucao> response = 
				this.restTemplate.postForEntity("http://localhost:" + port + "/devolucao", devolucaoDTO, Devolucao.class);
		
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(UUID.fromString("82cfcc93-dafc-4b5f-8ec1-5082fe57a4bd"), response.getBody().getBicicleta());
		assertEquals(UUID.fromString("b9a029d6-8ba1-4be7-9dcb-2468baeb4cb6"), response.getBody().getTrancaFim());
	}
	
	@Test
	void realizarDevolucao_TrancaSemAluguelCorrente_DeveLancarExcecao() throws UnirestException, JsonProcessingException {
		
		DevolucaoDTO devolucaoDTO = new DevolucaoDTO();
		devolucaoDTO.setIdBicicleta(UUID.fromString("82cfcc93-dafc-4b5f-8ec1-5082fe57a4bd"));
		devolucaoDTO.setIdTranca(UUID.fromString("0d3ed1c2-f6fe-49be-bb4c-cbb0d5ce4664"));
		
		ResponseEntity<ErrorResponse> response =
				this.restTemplate.postForEntity("http://localhost:" + port + "/devolucao", devolucaoDTO, ErrorResponse.class);
		
		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals("BAD400", response.getBody().getCodigo());
		assertEquals("Não foi possível encontrar a entidade solicitada", response.getBody().getMensagem());
		assertEquals("0d3ed1c2-f6fe-49be-bb4c-cbb0d5ce4664", response.getBody().getId());
	}
	
	@Test
	void realizarDevolucao_MetodoHTTPNaoPermitido_DeveLancarExcecao() throws UnirestException, JsonProcessingException {
		
		DevolucaoDTO devolucaoDTO = new DevolucaoDTO();
		devolucaoDTO.setIdBicicleta(UUID.fromString("82cfcc93-dafc-4b5f-8ec1-5082fe57a4bd"));
		devolucaoDTO.setIdTranca(UUID.fromString("0d3ed1c2-f6fe-49be-bb4c-cbb0d5ce4664"));
		
		ResponseEntity<ErrorResponse> response =
				this.restTemplate.getForEntity("http://localhost:" + port + "/devolucao", ErrorResponse.class);
		
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
		assertEquals("MET405", response.getBody().getCodigo());
		assertEquals("Request method 'GET' not supported", response.getBody().getMensagem());
	}

}
