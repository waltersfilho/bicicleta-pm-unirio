package br.unirio.bicicleta.integration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.controller.ciclista.CiclistaController;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.util.MockFactory;

@SpringBootTest
class CiclistaControllerTest {
	
	@Autowired
	private CiclistaController ciclistaController;
	
	@Test
	void testInserirCiclista_DeveInserirCorretamente() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
		
		assertNotNull(ciclistaController.cadastrarCiclista(cadastroCiclistaDTO));
	}
	
}
