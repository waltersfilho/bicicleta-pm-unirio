package br.unirio.bicicleta.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.ciclista.Passaporte;
import br.unirio.bicicleta.model.funcionario.Funcionario;

public class MockFactory {
	
	public static Date hoje = Date.from(
			LocalDate
			.of( 2022 , 1 , 14 )
			.atStartOfDay(
					ZoneId.of( "America/Fortaleza" ))
			.toInstant());
	
	public static Ciclista contruirCiclista() {
		Ciclista ciclista = new Ciclista();
		
		Passaporte passaporte = new Passaporte();
		
		passaporte.setNumero("123");
		passaporte.setPais("Brasil");
		passaporte.setValidade(hoje);
		
		ciclista.setCpf("11111111111");
		ciclista.setEmail("exame@exame.com");
		ciclista.setNacionalidade("Brasileiro");
		ciclista.setNascimento(hoje);
		ciclista.setNome("Jesse Pinkman");
		ciclista.setPassaporte(passaporte);
		ciclista.setSenha("123");
		ciclista.setStatus("Criado");
		
		return ciclista;
	}
	
	public static MeioDePagamento construirMeioDePagamento() {
		MeioDePagamento meioDePagamento = new MeioDePagamento();
		
		meioDePagamento.setCvv("123");
		meioDePagamento.setNomeTitular("Walter White");
		meioDePagamento.setNumero("1234123412341234");
		meioDePagamento.setValidade(hoje);
		meioDePagamento.setCiclista(contruirCiclista());
		
		return meioDePagamento;
	}
	
	public static Funcionario construirFuncionario() {
		Funcionario funcionario = new Funcionario();
		
		funcionario.setCpf("11111111111");
		funcionario.setEmail("exame@exame.com");
		funcionario.setFuncao("Encarregado");
		funcionario.setIdade(44);
		funcionario.setNome("Gus Fring");
		funcionario.setSenha("123");
		
		return funcionario;
	}

}
