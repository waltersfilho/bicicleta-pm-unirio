package br.unirio.bicicleta.unit.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.CPFObrigatorioException;
import br.unirio.bicicleta.exception.EmailExistenteException;
import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.exception.PassaporteInvalidoException;
import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCiclistaDTO;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.repository.ciclista.CiclistaRepository;
import br.unirio.bicicleta.repository.ciclista.MeioDePagamentoRepository;
import br.unirio.bicicleta.service.IEnvioEmailService;
import br.unirio.bicicleta.service.external.ICiclistaService;
import br.unirio.bicicleta.service.external.ICobrancaService;
import br.unirio.bicicleta.service.impl.CiclistaService;
import br.unirio.bicicleta.util.MockFactory;

class CiclistaServiceTest {
	
	private ICiclistaService ciclistaService;
	private ICobrancaService cobrancaService;
	private IEnvioEmailService envioEmailService;
	private CiclistaRepository ciclistaRepository;
	private MeioDePagamentoRepository meioDePagamentoRepository;
	private ModelMapper mapper;
	
	@BeforeEach
	void beforeTest() {
		
		ciclistaRepository = mock(CiclistaRepository.class);
		meioDePagamentoRepository = mock(MeioDePagamentoRepository.class);
		cobrancaService = mock(ICobrancaService.class);
		envioEmailService = mock(IEnvioEmailService.class);
		mapper = new ModelMapper();
						
		ciclistaService = new CiclistaService(ciclistaRepository, meioDePagamentoRepository, mapper, envioEmailService, cobrancaService);
	}
	
	@Test
	void testInserirCiclista() throws MetodoDePagamentoInvalidoException, UnirestException, JsonProcessingException {
		Ciclista ciclista = MockFactory.contruirCiclista();
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		when(ciclistaRepository.save(ciclista)).thenReturn(ciclista);
		
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
		
		ciclista = ciclistaService.inserirCiclista(cadastroCiclistaDTO);
		
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testBuscarPorID_CiclistaExistente_DeveRetornar() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.of(ciclista));
		
		ciclista = ciclistaService.buscarPorID(uuid);	
		
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testBuscarPorID_CiclistaNaoExistente_DeveDispararExcecao() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.empty());
		
		Throwable exception = assertThrows(ResourceNotFoundException.class, () -> ciclistaService.buscarPorID(uuid));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
	}
	
	@Test
	void testAtualizarCiclista_CiclistaExistente_DeveAtualizar() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		ciclista.setNome("Walter White");
		
		AtualizarCiclistaDTO atualizarCiclistaDTO = mapper.map(ciclista, AtualizarCiclistaDTO.class);
						
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.of(ciclista));
		when(ciclistaRepository.save(ciclista)).thenReturn(ciclista);
		
		ciclista = ciclistaService.atualizarCiclista(atualizarCiclistaDTO, uuid);
				
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Walter White", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testAtualizarCiclista_CiclistaNaoExistente_DeveDispararExcecao() {
		UUID uuid = UUID.randomUUID();
				
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		ciclista.setNome("Walter White");
		
		AtualizarCiclistaDTO atualizarCiclistaDTO = mapper.map(ciclista, AtualizarCiclistaDTO.class);
								
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.empty());
				
		Throwable exception = assertThrows(ResourceNotFoundException.class, () -> ciclistaService.atualizarCiclista(atualizarCiclistaDTO, uuid));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
				
		
	}
	
	@Test
	void testAtivarCiclista_CiclistaExistente_DeveAtivar() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.of(ciclista));
		when(ciclistaRepository.save(ciclista)).thenReturn(ciclista);
		
		ciclista = ciclistaService.ativarCiclista(uuid);
		
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Ativo", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testAtivarCiclista_CiclistaNaoExistente_DeveDispararException() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		
		when(ciclistaRepository.findById(uuid)).thenReturn(Optional.empty());
				
		Throwable exception = assertThrows(ResourceNotFoundException.class, () -> ciclistaService.ativarCiclista(uuid));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
		
	}
	
	@Test
	void testExisteEmail() {
		String email = "teste@example.com";
		
		when(ciclistaRepository.existsByEmail(email)).thenReturn(true);
				
		assertTrue(ciclistaService.existeEmail(email));
	}
	
	@Test
	void testInserirCiclista_EmailJaExistente_DeveLancarExcecao() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
		
		when(ciclistaRepository.existsByEmail(ciclista.getEmail())).thenReturn(true);
		
		Throwable exception = assertThrows(EmailExistenteException.class, () -> ciclistaService.inserirCiclista(cadastroCiclistaDTO));
		assertEquals("Email já cadastrado", exception.getMessage());	
	}
	
	@Test
	void testInserirCiclista_CPFVazioParaBrasileiro_DeveLancarExcecao() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setCpf("");
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
				
		Throwable exception = assertThrows(CPFObrigatorioException.class, () -> ciclistaService.inserirCiclista(cadastroCiclistaDTO));
		assertEquals("CPF obrigatório para pessoas brasileiras.", exception.getMessage());	
	}
	
	@Test
	void testInserirCiclista_CPFVazioParaNaoBrasileiro_DeveCadastrarCorretamente() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setCpf("");
		ciclista.setNacionalidade("Mexicano");
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		when(ciclistaRepository.save(ciclista)).thenReturn(ciclista);
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
				
		ciclista = ciclistaService.inserirCiclista(cadastroCiclistaDTO);
		
		assertEquals("", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Mexicano", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testInserirCiclista_CiclistaNaoBrasileiroEPassaporteNulo_DeveLancarExcecao() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setCpf("");
		ciclista.setNacionalidade("Mexicano");
		ciclista.setPassaporte(null);
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
				
		Throwable exception = assertThrows(PassaporteInvalidoException.class, () -> ciclistaService.inserirCiclista(cadastroCiclistaDTO));
		assertEquals("Dados de passaporte inválidos.", exception.getMessage());	
	}
	
	@Test
	void testInserirCiclista_CiclistaNaoBrasileiroEPassaporteComPaisVazio_DeveLancarExcecao() throws JsonProcessingException, UnirestException {
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setCpf("");
		ciclista.setNacionalidade("Mexicano");
		ciclista.getPassaporte().setPais("");
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
				
		Throwable exception = assertThrows(PassaporteInvalidoException.class, () -> ciclistaService.inserirCiclista(cadastroCiclistaDTO));
		assertEquals("Dados de passaporte inválidos.", exception.getMessage());	
	}

}
