package br.unirio.bicicleta.unit.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCartaoDeCreditoDTO;
import br.unirio.bicicleta.repository.ciclista.MeioDePagamentoRepository;
import br.unirio.bicicleta.service.IEnvioEmailService;
import br.unirio.bicicleta.service.IMeioDePagamentoService;
import br.unirio.bicicleta.service.impl.MeioDePagamentoService;
import br.unirio.bicicleta.util.MockFactory;

class MeioDePagamentoServiceTest {

	private IMeioDePagamentoService meioDePagamentoService;
	private IEnvioEmailService envioEmailService;
	private MeioDePagamentoRepository meioDePagamentoRepository;
	private ModelMapper mapper;

	@BeforeEach
	void beforeTest() {

		meioDePagamentoRepository = mock(MeioDePagamentoRepository.class);
		envioEmailService = mock(IEnvioEmailService.class);
		
		mapper = new ModelMapper();

		meioDePagamentoService = new MeioDePagamentoService(meioDePagamentoRepository, mapper, envioEmailService);
	}

	@Test
	void testRecuperarMeioDePagamento_MeioDePagamentoExistente_DeveRetornar() {
		UUID uuid = UUID.randomUUID();

		UUID idCiclista = UUID.randomUUID();

		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setId(uuid);

		when(meioDePagamentoRepository.findByCiclistaId(idCiclista)).thenReturn(Optional.of(meioDePagamento));

		meioDePagamento = meioDePagamentoService.recuperarMeioDePagamento(idCiclista);

		assertEquals(uuid, meioDePagamento.getId());
		assertEquals("123", meioDePagamento.getCvv());
		assertEquals("Walter White", meioDePagamento.getNomeTitular());
		assertEquals("1234123412341234", meioDePagamento.getNumero());
		assertEquals(MockFactory.hoje, meioDePagamento.getValidade());
	}

	@Test
	void testRecuperarMeioDePagamento_MeioDePagamentoNaoExistente_DeveDispararExcecao() {

		UUID uuid = UUID.randomUUID();

		UUID idCiclista = UUID.randomUUID();

		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setId(uuid);

		when(meioDePagamentoRepository.findByCiclistaId(idCiclista)).thenReturn(Optional.empty());

		Throwable exception = assertThrows(ResourceNotFoundException.class,
				() -> meioDePagamentoService.recuperarMeioDePagamento(idCiclista));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
	}

	@Test
	void testAtualizarMeioDePagamento_MeioDePagamentoExistente_DeveAtualizar() throws JsonProcessingException, UnirestException {
		UUID uuid = UUID.randomUUID();

		UUID idCiclista = UUID.randomUUID();

		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setCvv("500");

		AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO = mapper.map(meioDePagamento,
				AtualizarCartaoDeCreditoDTO.class);

		meioDePagamento.setId(uuid);

		when(meioDePagamentoRepository.findByCiclistaId(idCiclista)).thenReturn(Optional.of(meioDePagamento));
		when(meioDePagamentoRepository.save(meioDePagamento)).thenReturn(meioDePagamento);

		meioDePagamento = meioDePagamentoService.atualizarMeioDePagamento(atualizarCartaoDeCreditoDTO, idCiclista);

		assertEquals(uuid, meioDePagamento.getId());
		assertEquals("500", meioDePagamento.getCvv());
		assertEquals("Walter White", meioDePagamento.getNomeTitular());
		assertEquals("1234123412341234", meioDePagamento.getNumero());
		assertEquals(MockFactory.hoje, meioDePagamento.getValidade());
	}

	@Test
	void testAtualizarMeioDePagamento_MeioDePagamentoNaoExistente_DeveDispararExcecao() {
		UUID uuid = UUID.randomUUID();
		
		UUID idCiclista = UUID.randomUUID();
		
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setCvv("500");
		
		AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO = mapper.map(meioDePagamento, AtualizarCartaoDeCreditoDTO.class);
		
		meioDePagamento.setId(uuid);
		
		when(meioDePagamentoRepository.findByCiclistaId(idCiclista)).thenReturn(Optional.empty());
			
		Throwable exception = assertThrows(ResourceNotFoundException.class, () -> meioDePagamentoService.atualizarMeioDePagamento(atualizarCartaoDeCreditoDTO, idCiclista));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());	
	}

}
