package br.unirio.bicicleta.unit.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.dto.AtualizarFuncionarioDTO;
import br.unirio.bicicleta.model.dto.CadastroFuncionarioDTO;
import br.unirio.bicicleta.model.funcionario.Funcionario;
import br.unirio.bicicleta.repository.funcionario.FuncionarioRepository;
import br.unirio.bicicleta.service.IFuncionarioService;
import br.unirio.bicicleta.service.impl.FuncionarioService;
import br.unirio.bicicleta.util.MockFactory;

class FuncionarioServiceTest {

	private IFuncionarioService funcionarioService;
	private FuncionarioRepository funcionarioRepository;
	private ModelMapper mapper;

	@BeforeEach
	void beforeTest() {

		funcionarioRepository = mock(FuncionarioRepository.class);
		mapper = new ModelMapper();

		funcionarioService = new FuncionarioService(funcionarioRepository, mapper);
	}

	@Test
	void testInserirFuncionario() {

		Funcionario funcionario = MockFactory.construirFuncionario();

		when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);

		CadastroFuncionarioDTO cadastroFuncionarioDTO = mapper.map(funcionario, CadastroFuncionarioDTO.class);

		funcionario = funcionarioService.inserirFuncionario(cadastroFuncionarioDTO);

		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Encarregado", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}

	@Test
	void testAtualizarFuncionario_FuncionarioExistente_DeveAtualizar() {
		Long matricula = 1L;

		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setFuncao("Gerente");

		AtualizarFuncionarioDTO atualizarFuncionarioDTO = mapper.map(funcionario, AtualizarFuncionarioDTO.class);

		funcionario.setMatricula(matricula);

		when(funcionarioRepository.existsById(matricula)).thenReturn(true);
		when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);

		funcionario = funcionarioService.atualizarFuncionario(atualizarFuncionarioDTO, matricula);

		assertEquals(matricula, funcionario.getMatricula());
		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Gerente", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}

	@Test
	void testAtualizarFuncionario_FuncionarioNaoExistente_DeveDispararExcecao() {
		Long matricula = 1L;

		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setFuncao("Gerente");

		AtualizarFuncionarioDTO atualizarFuncionarioDTO = mapper.map(funcionario, AtualizarFuncionarioDTO.class);

		funcionario.setMatricula(matricula);

		when(funcionarioRepository.getById(matricula)).thenThrow(EntityNotFoundException.class);
		when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);

		Throwable exception = assertThrows(ResourceNotFoundException.class,
				() -> funcionarioService.atualizarFuncionario(atualizarFuncionarioDTO, matricula));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
	}
	
	@Test
	void testBuscarFuncionario_FuncionarioExistente_DeveRetornar() {
		Long matricula = 1L;
		
		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setMatricula(matricula);

		when(funcionarioRepository.findById(matricula)).thenReturn(Optional.of(funcionario));
		
		funcionario = funcionarioService.buscarPorID(matricula);

		assertEquals(matricula, funcionario.getMatricula());
		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Encarregado", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}
	
	@Test
	void testBuscarFuncionario_FuncionarioNaoExistente_DeveDispararExcecao() {
		Long matricula = 1L;

		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setMatricula(matricula);

		when(funcionarioRepository.findById(matricula)).thenReturn(Optional.empty());

		Throwable exception = assertThrows(ResourceNotFoundException.class,
				() -> funcionarioService.buscarPorID(matricula));
		assertEquals("Não foi possível encontrar a entidade solicitada", exception.getMessage());
	}
	
	@Test
	void testDeletarFuncionario() {		
		Long matricula = 1L;

		doNothing().when(funcionarioRepository).deleteById(matricula);
		
		funcionarioService.deletarFuncionario(matricula);
		
		verify(funcionarioRepository, times(1)).deleteById(matricula);
	}
	
	@Test
	void testRecuperarFuncionarios() {
		Long matricula = 1L;
		
		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setMatricula(matricula);
		
		List<Funcionario> funcionarios = new ArrayList<Funcionario>(Arrays.asList(funcionario));
		
		when(funcionarioRepository.findAll()).thenReturn(funcionarios);
		
		List<Funcionario> funcionariosRetornados = funcionarioService.recuperarFuncionarios();
		
		assertEquals(matricula, funcionariosRetornados.get(0).getMatricula());
		assertEquals("11111111111", funcionariosRetornados.get(0).getCpf());
		assertEquals("exame@exame.com", funcionariosRetornados.get(0).getEmail());
		assertEquals("Encarregado", funcionariosRetornados.get(0).getFuncao());
		assertEquals(44, funcionariosRetornados.get(0).getIdade());
		assertEquals("Gus Fring", funcionariosRetornados.get(0).getNome());
		assertEquals("123", funcionariosRetornados.get(0).getSenha());
		
	}

}
