package br.unirio.bicicleta.unit.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.controller.ciclista.CiclistaController;
import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCiclistaDTO;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.service.impl.CiclistaService;
import br.unirio.bicicleta.util.MockFactory;

class CiclistaControllerTest {
	
	private CiclistaController ciclistaController;
	private CiclistaService ciclistaService;
	private ModelMapper mapper;
	
	@BeforeEach
	void beforeTest() {
		ciclistaService = mock(CiclistaService.class);
		mapper = new ModelMapper();
		
		ciclistaController = new CiclistaController(ciclistaService);
	}
	
	@Test
	void testCadastrarCiclista() throws MetodoDePagamentoInvalidoException, UnirestException, JsonProcessingException {
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		
		CadastroCiclistaDTO cadastroCiclistaDTO = new CadastroCiclistaDTO();
		cadastroCiclistaDTO.setCiclista(ciclista);
		cadastroCiclistaDTO.setMeioDePagamento(meioDePagamento);
		
		when(ciclistaService.inserirCiclista(cadastroCiclistaDTO)).thenReturn(ciclista);
		
		ciclista = ciclistaController.cadastrarCiclista(cadastroCiclistaDTO);
		
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());	
	}
	
	@Test
	void testBuscarPorID() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		
		when(ciclistaService.buscarPorID(uuid)).thenReturn(ciclista);
		
		ciclista = ciclistaController.recuperarCiclista(uuid);	
		
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testAtualizarCiclista_CiclistaExistente_DeveAtualizar() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		ciclista.setNome("Walter White");
		
		AtualizarCiclistaDTO atualizarCiclistaDTO = mapper.map(ciclista, AtualizarCiclistaDTO.class);
						
		when(ciclistaService.atualizarCiclista(atualizarCiclistaDTO, uuid)).thenReturn(ciclista);
		
		ciclista = ciclistaController.atualizarCiclista(atualizarCiclistaDTO, uuid);
				
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Walter White", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Criado", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testAtivarCiclista() {
		UUID uuid = UUID.randomUUID();
		
		Ciclista ciclista = MockFactory.contruirCiclista();
		ciclista.setId(uuid);
		ciclista.setStatus("Ativo");
		
		when(ciclistaService.ativarCiclista(uuid)).thenReturn(ciclista);
		
		ciclista = ciclistaController.ativarCiclista(uuid);
				
		assertEquals(uuid, ciclista.getId());
		assertEquals("11111111111", ciclista.getCpf());
		assertEquals("exame@exame.com", ciclista.getEmail());
		assertEquals("Brasileiro", ciclista.getNacionalidade());
		assertEquals("Jesse Pinkman", ciclista.getNome());
		assertEquals("123", ciclista.getSenha());
		assertEquals("Ativo", ciclista.getStatus());
		assertEquals(MockFactory.hoje, ciclista.getNascimento());
		assertEquals("123", ciclista.getPassaporte().getNumero());
		assertEquals("Brasil", ciclista.getPassaporte().getPais());
		assertEquals(MockFactory.hoje, ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void testExisteEmail() {
		String email = "teste@example.com";
		
		when(ciclistaService.existeEmail(email)).thenReturn(true);
				
		assertTrue(ciclistaController.existeEmail(email));
	}

}
