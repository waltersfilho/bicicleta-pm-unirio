package br.unirio.bicicleta.unit.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import br.unirio.bicicleta.controller.funcionario.FuncionarioController;
import br.unirio.bicicleta.model.dto.AtualizarFuncionarioDTO;
import br.unirio.bicicleta.model.dto.CadastroFuncionarioDTO;
import br.unirio.bicicleta.model.funcionario.Funcionario;
import br.unirio.bicicleta.service.impl.FuncionarioService;
import br.unirio.bicicleta.util.MockFactory;

class FuncionarioControllerTest {
	
	private FuncionarioController funcionarioController;
	private FuncionarioService funcionarioService;
	private ModelMapper mapper;
	
	@BeforeEach
	void beforeTest() {
		
		funcionarioService = mock(FuncionarioService.class);
		mapper = new ModelMapper();
		
		funcionarioController = new FuncionarioController(funcionarioService);
	}
	
	@Test
	void testRecuperarFuncionarios() {
		
		Long matricula = 1L;
		
		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setMatricula(matricula);
		
		List<Funcionario> funcionarios = new ArrayList<Funcionario>(Arrays.asList(funcionario));
		
		when(funcionarioService.recuperarFuncionarios()).thenReturn(funcionarios);
		
		List<Funcionario> funcionariosRetornados = funcionarioController.recuperarFuncionarios();
		
		assertEquals(matricula, funcionariosRetornados.get(0).getMatricula());
		assertEquals("11111111111", funcionariosRetornados.get(0).getCpf());
		assertEquals("exame@exame.com", funcionariosRetornados.get(0).getEmail());
		assertEquals("Encarregado", funcionariosRetornados.get(0).getFuncao());
		assertEquals(44, funcionariosRetornados.get(0).getIdade());
		assertEquals("Gus Fring", funcionariosRetornados.get(0).getNome());
		assertEquals("123", funcionariosRetornados.get(0).getSenha());
		
	}
	
	@Test
	void testRecuperarFuncionario() {
		Long matricula = 1L;
		
		Funcionario funcionario = MockFactory.construirFuncionario();
		funcionario.setMatricula(matricula);
		
		when(funcionarioService.buscarPorID(matricula)).thenReturn(funcionario);
		
		funcionario = funcionarioController.recuperarFuncionario(matricula);
		
		assertEquals(matricula, funcionario.getMatricula());
		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Encarregado", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}
	
	@Test
	void cadastrarFuncionario() {
		Funcionario funcionario = MockFactory.construirFuncionario();
		
		CadastroFuncionarioDTO cadastroFuncionarioDTO = mapper.map(funcionario, CadastroFuncionarioDTO.class);
		
		when(funcionarioService.inserirFuncionario(cadastroFuncionarioDTO)).thenReturn(funcionario);
		
		funcionario = funcionarioController.cadastrarFuncionario(cadastroFuncionarioDTO);
		
		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Encarregado", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}
	
	@Test
	void atualizarFuncionario() {
		Funcionario funcionario = MockFactory.construirFuncionario();
		Long matricula = 1L;
		
		AtualizarFuncionarioDTO atualizarFuncionarioDTO = mapper.map(funcionario, AtualizarFuncionarioDTO.class);
		
		funcionario.setMatricula(matricula);
		
		when(funcionarioService.atualizarFuncionario(atualizarFuncionarioDTO, matricula)).thenReturn(funcionario);
		
		funcionario = funcionarioController.atualizarFuncionario(atualizarFuncionarioDTO, matricula);
		
		assertEquals(matricula, funcionario.getMatricula());
		assertEquals("11111111111", funcionario.getCpf());
		assertEquals("exame@exame.com", funcionario.getEmail());
		assertEquals("Encarregado", funcionario.getFuncao());
		assertEquals(44, funcionario.getIdade());
		assertEquals("Gus Fring", funcionario.getNome());
		assertEquals("123", funcionario.getSenha());
	}
	
	@Test
	void deletarFuncionario() {
		Long matricula = 1L;

		doNothing().when(funcionarioService).deletarFuncionario(matricula);
		
		funcionarioController.deletarFuncionario(matricula);
		
		verify(funcionarioService, times(1)).deletarFuncionario(matricula);
	}
	

}
