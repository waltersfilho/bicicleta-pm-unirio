package br.unirio.bicicleta.unit.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.controller.meiodepagamento.MeioDePagamentoController;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCartaoDeCreditoDTO;
import br.unirio.bicicleta.service.impl.MeioDePagamentoService;
import br.unirio.bicicleta.util.MockFactory;

class MeioDePagamentoControllerTest {
	
	private MeioDePagamentoController meioDePagamentoController;
	private MeioDePagamentoService meioDePagamentoService;
	private ModelMapper mapper;
	
	@BeforeEach
	void beforeTest() {
		meioDePagamentoService = mock(MeioDePagamentoService.class);
		mapper = new ModelMapper();
		
		meioDePagamentoController = new MeioDePagamentoController(meioDePagamentoService);
	}
	
	@Test
	void testRecuperarMeioDePagamento() {
		UUID uuid = UUID.randomUUID();

		UUID idCiclista = UUID.randomUUID();

		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setId(uuid);

		when(meioDePagamentoService.recuperarMeioDePagamento(idCiclista)).thenReturn(meioDePagamento);

		meioDePagamento = meioDePagamentoController.recuperarCartaoDeCredito(idCiclista);

		assertEquals(uuid, meioDePagamento.getId());
		assertEquals("123", meioDePagamento.getCvv());
		assertEquals("Walter White", meioDePagamento.getNomeTitular());
		assertEquals("1234123412341234", meioDePagamento.getNumero());
		assertEquals(MockFactory.hoje, meioDePagamento.getValidade());
	}
	
	@Test
	void testAtualizarMeioDePagamento_MeioDePagamentoExistente_DeveAtualizar() throws JsonProcessingException, UnirestException {
		UUID uuid = UUID.randomUUID();

		UUID idCiclista = UUID.randomUUID();

		MeioDePagamento meioDePagamento = MockFactory.construirMeioDePagamento();
		meioDePagamento.setCvv("500");

		AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO = mapper.map(meioDePagamento,
				AtualizarCartaoDeCreditoDTO.class);

		meioDePagamento.setId(uuid);

		when(meioDePagamentoService.atualizarMeioDePagamento(atualizarCartaoDeCreditoDTO, idCiclista)).thenReturn(meioDePagamento);

		meioDePagamento = meioDePagamentoController.atualizarCartaoDeCredito(atualizarCartaoDeCreditoDTO, idCiclista);

		assertEquals(uuid, meioDePagamento.getId());
		assertEquals("500", meioDePagamento.getCvv());
		assertEquals("Walter White", meioDePagamento.getNomeTitular());
		assertEquals("1234123412341234", meioDePagamento.getNumero());
		assertEquals(MockFactory.hoje, meioDePagamento.getValidade());
	}
	
	

}
