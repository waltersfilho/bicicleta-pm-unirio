package br.unirio.bicicleta.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ErrorResponse {
	
	private Object id;
	private String codigo;
	private String mensagem;

}
