package br.unirio.bicicleta; 

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.unirio.bicicleta.config.CustomProperties;

@SpringBootApplication
@EnableConfigurationProperties(CustomProperties.class)
public class AluguelApplication {

	public static void main(String[] args) {
		SpringApplication.run(AluguelApplication.class, args); 
	}

}
