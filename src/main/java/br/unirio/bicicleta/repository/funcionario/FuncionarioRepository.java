package br.unirio.bicicleta.repository.funcionario;

import org.springframework.data.jpa.repository.JpaRepository;

import br.unirio.bicicleta.model.funcionario.Funcionario;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

}
