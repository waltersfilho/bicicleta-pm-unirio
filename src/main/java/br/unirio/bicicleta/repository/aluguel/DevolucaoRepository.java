package br.unirio.bicicleta.repository.aluguel;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import br.unirio.bicicleta.model.aluguel.Devolucao;

public interface DevolucaoRepository extends JpaRepository<Devolucao, UUID> {

}
