package br.unirio.bicicleta.repository.aluguel;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import br.unirio.bicicleta.model.aluguel.Aluguel;

public interface AluguelRepository extends JpaRepository<Aluguel, UUID> {
	
	public boolean existsByCiclista(UUID ciclista);
	
	public Aluguel findByBicicletaAndTrancaFimIsNull(UUID bicicleta);

}
