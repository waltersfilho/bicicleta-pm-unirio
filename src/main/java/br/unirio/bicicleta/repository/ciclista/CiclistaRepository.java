package br.unirio.bicicleta.repository.ciclista;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import br.unirio.bicicleta.model.ciclista.Ciclista;

public interface CiclistaRepository extends JpaRepository<Ciclista, UUID> {
	
	public Boolean existsByEmail(String email);

}
