package br.unirio.bicicleta.repository.ciclista;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.unirio.bicicleta.model.ciclista.MeioDePagamento;

public interface MeioDePagamentoRepository extends JpaRepository<MeioDePagamento, UUID> {
	
	@Query("from MeioDePagamento mp join mp.ciclista c where c.id = ?1")
	Optional<MeioDePagamento> findByCiclistaId(UUID idCiclista);

}
