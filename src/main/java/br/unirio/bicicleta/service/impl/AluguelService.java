package br.unirio.bicicleta.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.AluguelEmAndamentoException;
import br.unirio.bicicleta.exception.BicicletaNaoDisponivelException;
import br.unirio.bicicleta.exception.BicicletaNaoHabilitadaException;
import br.unirio.bicicleta.exception.CiclistaNaoPossuiCadastroAtivoException;
import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.exception.TrancaNaoDisponivelException;
import br.unirio.bicicleta.exception.TrancaVaziaException;
import br.unirio.bicicleta.model.aluguel.Aluguel;
import br.unirio.bicicleta.model.aluguel.Devolucao;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.dto.AluguelDTO;
import br.unirio.bicicleta.model.dto.BicicletaDTO;
import br.unirio.bicicleta.model.dto.CobrancaDTO;
import br.unirio.bicicleta.model.dto.DevolucaoDTO;
import br.unirio.bicicleta.model.dto.TrancaDTO;
import br.unirio.bicicleta.repository.aluguel.AluguelRepository;
import br.unirio.bicicleta.repository.aluguel.DevolucaoRepository;
import br.unirio.bicicleta.repository.ciclista.CiclistaRepository;
import br.unirio.bicicleta.service.IAluguelService;
import br.unirio.bicicleta.service.IEnvioEmailService;
import br.unirio.bicicleta.service.external.IBicicletaService;
import br.unirio.bicicleta.service.external.ICobrancaService;
import br.unirio.bicicleta.service.external.ITrancaService;

@Service
public class AluguelService implements IAluguelService {

	private static final String LIVRE = "LIVRE";

	private static final String EM_USO = "EM_USO";

	private static final String DISPONIVEL = "DISPONIVEL";

	private static final double VALOR_MEIA_HORA = 5.00;

	private static final double VALOR_COBRANCA = 10.00;

	@Autowired
	private AluguelRepository aluguelRepository;

	@Autowired
	private CiclistaRepository ciclistaRepository;

	@Autowired
	private DevolucaoRepository devolucaoRepository;

	@Autowired
	private IEnvioEmailService envioEmailService;

	@Autowired
	private ITrancaService trancaService;
	
	@Autowired
	private IBicicletaService bicicletaService;
	
	@Autowired
	private ICobrancaService cobrancaService;

	@Override
	public Aluguel realizarAluguel(AluguelDTO aluguelDTO) throws UnirestException, JsonProcessingException {

		Ciclista ciclista = ciclistaRepository.findById(aluguelDTO.getCiclista())
				.orElseThrow(() -> new ResourceNotFoundException(aluguelDTO.getCiclista()));

		if (!ciclista.getStatus().equals("Ativo")) {
			throw new CiclistaNaoPossuiCadastroAtivoException(ciclista.getId());
		}

		if (aluguelRepository.existsByCiclista(aluguelDTO.getCiclista())) {
			throw new AluguelEmAndamentoException(aluguelDTO.getCiclista());
		}

		TrancaDTO trancaDTO = trancaService.getTranca(aluguelDTO.getTrancaInicio());
		
		if(!trancaDTO.getStatus().equals("OCUPADA")) {
			throw new TrancaVaziaException(trancaDTO.getId());
		}

		BicicletaDTO bicicletaDTO = bicicletaService.getBicicleta(trancaDTO.getBicicleta());

		if (!bicicletaDTO.getStatus().equals(DISPONIVEL)) {
			throw new BicicletaNaoDisponivelException(bicicletaDTO.getId());
		}

		CobrancaDTO cobrancaDTO = cobrancaService.realizarCobranca(aluguelDTO.getCiclista(), VALOR_COBRANCA);

		Date dataAgora = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(dataAgora);
		cal.add(Calendar.HOUR_OF_DAY, 2);

		Date dataDuasHoras = cal.getTime();

		Aluguel aluguel = new Aluguel();
		aluguel.setBicicleta(trancaDTO.getBicicleta());
		aluguel.setCiclista(aluguelDTO.getCiclista());
		aluguel.setCobranca(cobrancaDTO.getId());
		aluguel.setHoraInicio(dataAgora);
		aluguel.setHoraFim(dataDuasHoras);
		aluguel.setTrancaInicio(aluguelDTO.getTrancaInicio());

		bicicletaService.mudarStatusBicicleta(trancaDTO.getBicicleta(), EM_USO);
		trancaService.mudarStatusTranca(aluguelDTO.getTrancaInicio(), LIVRE);

		aluguelRepository.save(aluguel);

		envioEmailService.enviarEmail(ciclista.getEmail(), "Aluguel realizado com sucesso");

		return aluguel;
	}

	@Override
	public Devolucao realizarDevolucao(DevolucaoDTO devolucaoDTO) throws JsonProcessingException, UnirestException {
		Aluguel aluguel = aluguelRepository.findByBicicletaAndTrancaFimIsNull(devolucaoDTO.getIdBicicleta());

		if (aluguel == null) {
			throw new ResourceNotFoundException(devolucaoDTO.getIdTranca());
		}

		Ciclista ciclista = ciclistaRepository.findById(aluguel.getCiclista())
				.orElseThrow(() -> new ResourceNotFoundException(aluguel.getCiclista()));

		TrancaDTO trancaFimDTO = trancaService.getTranca(devolucaoDTO.getIdTranca());

		if (!trancaFimDTO.getStatus().equals(LIVRE)) {
			throw new TrancaNaoDisponivelException(trancaFimDTO.getId());
		}

		BicicletaDTO biclicletaDTO = bicicletaService.getBicicleta(devolucaoDTO.getIdBicicleta());

		if (!biclicletaDTO.getStatus().equals(EM_USO)) {
			throw new BicicletaNaoHabilitadaException(biclicletaDTO.getId());
		}

		Date dataAgora = new Date();
		Devolucao devolucao = new Devolucao();

		long faixasACobrar = this.getDateDiff(dataAgora, aluguel.getHoraInicio(), TimeUnit.MINUTES) / 30;

		if (faixasACobrar > 4) {
			Double valorACobrar = faixasACobrar * VALOR_MEIA_HORA;

			CobrancaDTO cobrancaDTO = cobrancaService.realizarCobranca(devolucaoDTO.getIdBicicleta(), valorACobrar);
			devolucao.setCobranca(cobrancaDTO.getId());
		}

		aluguel.setHoraFim(dataAgora);
		aluguel.setTrancaFim(devolucaoDTO.getIdTranca());

		aluguelRepository.save(aluguel);

		devolucao.setBicicleta(devolucaoDTO.getIdBicicleta());
		devolucao.setCiclista(aluguel.getCiclista());
		devolucao.setHoraInicio(aluguel.getHoraInicio());
		devolucao.setHoraFim(dataAgora);
		devolucao.setTrancaFim(devolucaoDTO.getIdTranca());

		trancaService.mudarStatusTranca(devolucaoDTO.getIdTranca(), "OCUPADA");
		bicicletaService.mudarStatusBicicleta(devolucaoDTO.getIdBicicleta(), DISPONIVEL);

		envioEmailService.enviarEmail(ciclista.getEmail(), "Devolução realizada com sucesso");

		devolucaoRepository.save(devolucao);

		return devolucao;
	}

	private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date1.getTime() - date2.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

}