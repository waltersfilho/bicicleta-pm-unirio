package br.unirio.bicicleta.service.impl;

import java.util.UUID;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.CPFObrigatorioException;
import br.unirio.bicicleta.exception.EmailExistenteException;
import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.exception.PassaporteInvalidoException;
import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.dto.AtualizarCiclistaDTO;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.repository.ciclista.CiclistaRepository;
import br.unirio.bicicleta.repository.ciclista.MeioDePagamentoRepository;
import br.unirio.bicicleta.service.IEnvioEmailService;
import br.unirio.bicicleta.service.external.ICiclistaService;
import br.unirio.bicicleta.service.external.ICobrancaService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CiclistaService implements ICiclistaService {

	private final CiclistaRepository ciclistaRepository;
	private MeioDePagamentoRepository meioDePagamentoRepository;
	private ModelMapper mapper;
	
	private IEnvioEmailService envioEmailService;
		
	private ICobrancaService cobrancaService;

	public Ciclista inserirCiclista(CadastroCiclistaDTO cadastroCiclistaDTO) throws MetodoDePagamentoInvalidoException, UnirestException, JsonProcessingException {
		
		cobrancaService.validarMetodoPagamento(cadastroCiclistaDTO);
		
		if(this.existeEmail(cadastroCiclistaDTO.getCiclista().getEmail())) {
			throw new EmailExistenteException(cadastroCiclistaDTO.getCiclista().getEmail());
		}
		
		if(cadastroCiclistaDTO.getCiclista().getNacionalidade().equals("Brasileiro")
				&& cadastroCiclistaDTO.getCiclista().getCpf().isEmpty()) {
			throw new CPFObrigatorioException();
		}
		
		else if (cadastroCiclistaDTO.getCiclista().getPassaporte() == null 
				|| cadastroCiclistaDTO.getCiclista().getPassaporte().getPais().isEmpty()) {
			throw new PassaporteInvalidoException();
		}

		cadastroCiclistaDTO.getMeioDePagamento().setCiclista(cadastroCiclistaDTO.getCiclista());

		Ciclista ciclistaRetorno = ciclistaRepository.save(cadastroCiclistaDTO.getCiclista());

		meioDePagamentoRepository.save(cadastroCiclistaDTO.getMeioDePagamento());
		
		envioEmailService.enviarEmail(cadastroCiclistaDTO.getCiclista().getEmail(), "Ciclista cadastrado com sucesso");
		
		return ciclistaRetorno;
	}


	public Ciclista buscarPorID(UUID id) {
		
		return ciclistaRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));

	}

	public Ciclista atualizarCiclista(AtualizarCiclistaDTO atualizarCiclistaDTO, UUID idCiclista) {

		Ciclista ciclistaAAtualizar = ciclistaRepository.findById(idCiclista)
				.orElseThrow(() -> new ResourceNotFoundException(idCiclista));

		Ciclista ciclista = mapper.map(atualizarCiclistaDTO, Ciclista.class);

		ciclista.setId(idCiclista);
		ciclista.setSenha(ciclistaAAtualizar.getSenha());

		return ciclistaRepository.save(ciclista);
	}

	public Ciclista ativarCiclista(UUID id) throws EntityNotFoundException {

		Ciclista ciclistaAAtualizar = ciclistaRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException(id));

		ciclistaAAtualizar.setStatus("Ativo");

		return ciclistaRepository.save(ciclistaAAtualizar);

	}

	public boolean existeEmail(String email) {

		return ciclistaRepository.existsByEmail(email);
	}
}
