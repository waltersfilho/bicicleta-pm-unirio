package br.unirio.bicicleta.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.dto.AtualizarFuncionarioDTO;
import br.unirio.bicicleta.model.dto.CadastroFuncionarioDTO;
import br.unirio.bicicleta.model.funcionario.Funcionario;
import br.unirio.bicicleta.repository.funcionario.FuncionarioRepository;
import br.unirio.bicicleta.service.IFuncionarioService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FuncionarioService implements IFuncionarioService {

	private FuncionarioRepository funcionarioRepository;
	private ModelMapper mapper;

	@Override
	public Funcionario inserirFuncionario(CadastroFuncionarioDTO cadastroFuncionarioDTO) {

		Funcionario funcionario = mapper.map(cadastroFuncionarioDTO, Funcionario.class);

		return funcionarioRepository.save(funcionario);
	}

	@Override
	public Funcionario buscarPorID(Long id) {
		
		return funcionarioRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));

	}

	@Override
	public Funcionario atualizarFuncionario(AtualizarFuncionarioDTO atualizarFuncionarioDTO, Long idFuncionario) {

		Funcionario funcionario = mapper.map(atualizarFuncionarioDTO, Funcionario.class);

		if (funcionarioRepository.existsById(idFuncionario)) {

			funcionario.setMatricula(idFuncionario);

			return funcionarioRepository.save(funcionario);

		} else {

			throw new ResourceNotFoundException(idFuncionario);
		}

	}

	@Override
	public void deletarFuncionario(Long id) {

		funcionarioRepository.deleteById(id);
	}

	@Override
	public List<Funcionario> recuperarFuncionarios() {

		return funcionarioRepository.findAll();
	}

}
