package br.unirio.bicicleta.service.impl;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCartaoDeCreditoDTO;
import br.unirio.bicicleta.repository.ciclista.MeioDePagamentoRepository;
import br.unirio.bicicleta.service.IEnvioEmailService;
import br.unirio.bicicleta.service.IMeioDePagamentoService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MeioDePagamentoService implements IMeioDePagamentoService {
	
	private MeioDePagamentoRepository meioDePagamentoRepository;
	private ModelMapper mapper;
	private IEnvioEmailService envioEmailService;

	@Override
	public MeioDePagamento recuperarMeioDePagamento(UUID idCiclista) {

		return meioDePagamentoRepository.findByCiclistaId(idCiclista)
				.orElseThrow(() -> new ResourceNotFoundException(idCiclista));
	}

	@Override
	public MeioDePagamento atualizarMeioDePagamento(AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO,
			UUID idCiclista) throws JsonProcessingException, UnirestException {

		MeioDePagamento meioDePagamentoAAtualizar = meioDePagamentoRepository.findByCiclistaId(idCiclista)
				.orElseThrow(() -> new ResourceNotFoundException(idCiclista));

		MeioDePagamento cartaoDeCredito = mapper.map(atualizarCartaoDeCreditoDTO, MeioDePagamento.class);

		cartaoDeCredito.setCiclista(meioDePagamentoAAtualizar.getCiclista());
		cartaoDeCredito.setId(meioDePagamentoAAtualizar.getId());
		
		envioEmailService.enviarEmail(cartaoDeCredito.getCiclista().getEmail(), "Cartão de Crédito Enviado com Sucesso");

		return meioDePagamentoRepository.save(cartaoDeCredito);

	}

}
