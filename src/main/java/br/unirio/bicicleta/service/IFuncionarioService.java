package br.unirio.bicicleta.service;

import java.util.List;

import br.unirio.bicicleta.model.dto.AtualizarFuncionarioDTO;
import br.unirio.bicicleta.model.dto.CadastroFuncionarioDTO;
import br.unirio.bicicleta.model.funcionario.Funcionario;

public interface IFuncionarioService {
	
	public Funcionario inserirFuncionario(CadastroFuncionarioDTO cadastroFuncionarioDTO);
	
	public Funcionario buscarPorID(Long id);
	
	public Funcionario atualizarFuncionario(AtualizarFuncionarioDTO atualizarFuncionarioDTO, Long idFuncionario);
	
	public void deletarFuncionario(Long id);
	
	public List<Funcionario> recuperarFuncionarios();
	
}
