package br.unirio.bicicleta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.aluguel.Aluguel;
import br.unirio.bicicleta.model.aluguel.Devolucao;
import br.unirio.bicicleta.model.dto.AluguelDTO;
import br.unirio.bicicleta.model.dto.DevolucaoDTO;

public interface IAluguelService  {
	
	public Aluguel realizarAluguel(AluguelDTO aluguelDTO) throws UnirestException, JsonProcessingException;
	
	public Devolucao realizarDevolucao(DevolucaoDTO devolucaoDTO) throws UnirestException, JsonProcessingException;

}
