package br.unirio.bicicleta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;


public interface IEnvioEmailService {
	
	public boolean enviarEmail(String email, String mensagem) throws UnirestException, JsonProcessingException;

}
