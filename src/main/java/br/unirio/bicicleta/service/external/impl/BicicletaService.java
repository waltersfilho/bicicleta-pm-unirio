package br.unirio.bicicleta.service.external.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.config.CustomProperties;
import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.model.dto.BicicletaDTO;
import br.unirio.bicicleta.service.external.IBicicletaService;

@Service
public class BicicletaService implements IBicicletaService {

	private static final String APPLICATION_JSON = "application/json";

	private static final String CONTENT_TYPE = "Content-Type";
	
	@Autowired
	private CustomProperties customProperties;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public boolean mudarStatusBicicleta(UUID bicicletaId, String status) throws UnirestException {
		
		HttpResponse<JsonNode> cobrancaResponse 
		  = Unirest.post(customProperties.getHostEquipamento()+"/bicicleta/"+bicicletaId+"/status/"+status)
		  .header(CONTENT_TYPE, APPLICATION_JSON)
		  .asJson();
		
		return cobrancaResponse.getStatus() == 200;
	}
	
	public BicicletaDTO getBicicleta(UUID bicicletaId) throws UnirestException, JsonProcessingException {
		
		HttpResponse<JsonNode> bicicletaResponse 
		  = Unirest.get(customProperties.getHostEquipamento()+"/bicicleta/"+bicicletaId.toString())
		  .asJson();
		
		if(bicicletaResponse.getStatus() == 404) {
			throw new ResourceNotFoundException(bicicletaId);
		}
		
		return objectMapper.readValue(bicicletaResponse.getBody().getObject().toString(), BicicletaDTO.class);	
	}
}
