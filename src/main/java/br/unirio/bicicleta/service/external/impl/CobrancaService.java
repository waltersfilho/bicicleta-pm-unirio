package br.unirio.bicicleta.service.external.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.config.CustomProperties;
import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.model.dto.CobrancaDTO;
import br.unirio.bicicleta.model.dto.RealizarCobrancaDTO;
import br.unirio.bicicleta.service.external.ICobrancaService;

@Service
public class CobrancaService implements ICobrancaService {
	
	private static final String APPLICATION_JSON = "application/json";

	private static final String CONTENT_TYPE = "Content-Type";
	
	@Autowired
	private CustomProperties customProperties;

	private ObjectMapper objectMapper = new ObjectMapper();
	
	public void validarMetodoPagamento(CadastroCiclistaDTO cadastroCiclistaDTO)
			throws UnirestException, MetodoDePagamentoInvalidoException, JsonProcessingException {
		
        String cadastroCiclistaDTOParsed = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(cadastroCiclistaDTO.getMeioDePagamento());
        
		HttpResponse<String> validaCartaoResponse = Unirest.post(customProperties.getHostCobranca()+"/validaCartaoDeCredito")
				.header(CONTENT_TYPE, APPLICATION_JSON)
				.body(cadastroCiclistaDTOParsed)
				.asString();
		
		if(validaCartaoResponse.getStatus() != 200) {
			throw new MetodoDePagamentoInvalidoException();
		}
	}
	
	public CobrancaDTO realizarCobranca(UUID idCiclista, Double valorCobranca)
			throws UnirestException, JsonProcessingException {
		RealizarCobrancaDTO realizarCobrancaDTO = new RealizarCobrancaDTO(valorCobranca, idCiclista);

		String realizarCobrancaDTOParsed = objectMapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(realizarCobrancaDTO);

		HttpResponse<JsonNode> cobrancaResponse = Unirest.post(customProperties.getHostCobranca() + "/cobranca")
				.header(CONTENT_TYPE, APPLICATION_JSON).body(realizarCobrancaDTOParsed).asJson();

		return objectMapper.readValue(cobrancaResponse.getBody().getObject().toString(), CobrancaDTO.class);
	}

}
