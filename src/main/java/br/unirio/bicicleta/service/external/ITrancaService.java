package br.unirio.bicicleta.service.external;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.dto.TrancaDTO;

public interface ITrancaService {
	
	public TrancaDTO getTranca(UUID idTranca) throws UnirestException, JsonProcessingException;
	
	public boolean mudarStatusTranca(UUID trancaId, String status) throws UnirestException;

}
