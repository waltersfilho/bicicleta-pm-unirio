package br.unirio.bicicleta.service.external;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.dto.BicicletaDTO;

public interface IBicicletaService {
	
	public boolean mudarStatusBicicleta(UUID bicicletaId, String status) throws UnirestException;
	
	public BicicletaDTO getBicicleta(UUID bicicletaId) throws UnirestException, JsonProcessingException;

}
