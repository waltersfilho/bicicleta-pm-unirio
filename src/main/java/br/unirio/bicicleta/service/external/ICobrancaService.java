package br.unirio.bicicleta.service.external;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.model.dto.CobrancaDTO;

public interface ICobrancaService {
	
	public void validarMetodoPagamento(CadastroCiclistaDTO cadastroCiclistaDTO)
			throws UnirestException, MetodoDePagamentoInvalidoException, JsonProcessingException;
	
	public CobrancaDTO realizarCobranca(UUID idCiclista, Double valorCobranca)
			throws UnirestException, JsonProcessingException;
}
