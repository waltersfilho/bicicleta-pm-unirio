package br.unirio.bicicleta.service.external;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.dto.AtualizarCiclistaDTO;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;


public interface ICiclistaService {
	
	public Ciclista inserirCiclista(CadastroCiclistaDTO cadastroCiclistaDTO) throws MetodoDePagamentoInvalidoException, UnirestException, JsonProcessingException;
	
	public Ciclista buscarPorID(UUID id);
	
	public Ciclista atualizarCiclista(AtualizarCiclistaDTO atualizarCiclistaDTO, UUID idCiclista);
	
	public Ciclista ativarCiclista(UUID id);
	
	public boolean existeEmail(String email);

}
