package br.unirio.bicicleta.service.external.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.config.CustomProperties;
import br.unirio.bicicleta.model.dto.EnviarEmailDTO;
import br.unirio.bicicleta.service.IEnvioEmailService;

@Service
public class EnvioEmailService implements IEnvioEmailService{
	
	private static final String APPLICATION_JSON = "application/json";

	private static final String CONTENT_TYPE = "Content-Type";
	
	@Autowired
	private CustomProperties customProperties;
	
	public boolean enviarEmail(String email, String mensagem)
			throws UnirestException, JsonProcessingException {
				
		EnviarEmailDTO enviarEmailDTO = new EnviarEmailDTO(email, mensagem);
		
        String cadastroCiclistaDTOParsed = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(enviarEmailDTO);
		
		HttpResponse<JsonNode> cobrancaResponse 
		  = Unirest.post(customProperties.getHostCobranca()+"/enviarEmail")
		  .header(CONTENT_TYPE, APPLICATION_JSON)
		  .body(cadastroCiclistaDTOParsed)
		  .asJson();
				
		return cobrancaResponse.getStatus() == 200;
	}

}
