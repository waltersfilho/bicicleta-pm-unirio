package br.unirio.bicicleta.service;

import java.util.UUID;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCartaoDeCreditoDTO;

public interface IMeioDePagamentoService {
	
	public MeioDePagamento recuperarMeioDePagamento(UUID idCiclista);
	
	public MeioDePagamento atualizarMeioDePagamento(AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO, UUID idCiclista) 
			throws JsonProcessingException, UnirestException;

}
