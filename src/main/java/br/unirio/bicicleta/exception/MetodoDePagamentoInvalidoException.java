package br.unirio.bicicleta.exception;

public class MetodoDePagamentoInvalidoException extends RuntimeException{

	private static final long serialVersionUID = 600218294437998894L;
	
	public MetodoDePagamentoInvalidoException() {
		super("Método de Pagamento Inválido");
	}

}
