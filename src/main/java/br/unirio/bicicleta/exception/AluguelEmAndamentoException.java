package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class AluguelEmAndamentoException extends RuntimeException {

	private static final long serialVersionUID = 7888797076773729252L;
	
	private final UUID id;

	public AluguelEmAndamentoException(UUID id) {
		super("O ciclista já possui um aluguel em andamento");
		this.id = id;
	}

}
