package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class TrancaNaoDisponivelException extends RuntimeException {

	private static final long serialVersionUID = -1140679196145961017L;
	
	private final UUID id;
	
	public TrancaNaoDisponivelException(UUID id) {
		super("Tranca informada não está disponível");
		this.id = id;
	}

}
