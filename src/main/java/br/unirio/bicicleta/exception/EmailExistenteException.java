package br.unirio.bicicleta.exception;

import javax.validation.ValidationException;

import lombok.Getter;

@Getter
public class EmailExistenteException extends ValidationException{

	private static final long serialVersionUID = 6019530190189209866L;
	
	private final String email;
	
	public EmailExistenteException(String email) {
		super("Email já cadastrado");
		this.email = email;
	}

}
