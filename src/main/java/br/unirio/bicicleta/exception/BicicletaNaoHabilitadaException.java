package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class BicicletaNaoHabilitadaException extends RuntimeException {

	private static final long serialVersionUID = 6607937513591530404L;
	
	private final UUID id;

    public BicicletaNaoHabilitadaException(UUID id) {
        super("Bicicleta não habilitada para a devolução");
        this.id = id;
    }

}