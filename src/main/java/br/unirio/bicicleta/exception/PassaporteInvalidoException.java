package br.unirio.bicicleta.exception;

import lombok.Getter;

@Getter
public class PassaporteInvalidoException extends RuntimeException {

	private static final long serialVersionUID = -8634033477672704394L;

	public PassaporteInvalidoException() {
        super("Dados de passaporte inválidos.");
    }

}