package br.unirio.bicicleta.exception;

import lombok.Getter;

@Getter
public class CPFObrigatorioException extends RuntimeException {

	private static final long serialVersionUID = -8082734299796789708L;

	public CPFObrigatorioException() {
    	super("CPF obrigatório para pessoas brasileiras.");
    }
}
