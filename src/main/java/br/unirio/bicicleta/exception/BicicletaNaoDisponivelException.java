package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class BicicletaNaoDisponivelException extends RuntimeException {
	
	private static final long serialVersionUID = 6303798189883390699L;
	
	private final UUID id;

	public BicicletaNaoDisponivelException(UUID id) {
		super("A bicicleta solicitada não está disponível");
		this.id = id;
	}
}
