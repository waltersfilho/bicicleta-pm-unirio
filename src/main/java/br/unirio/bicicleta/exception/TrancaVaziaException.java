package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class TrancaVaziaException extends RuntimeException {

	private static final long serialVersionUID = -3670809897650238314L;
	
	private final UUID id;

	public TrancaVaziaException(UUID id) {
		super("A tranca solicitada está vazia");
		this.id = id;
	}

}
