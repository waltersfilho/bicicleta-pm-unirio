package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class CiclistaNaoPossuiCadastroAtivoException extends RuntimeException{
	
	private static final long serialVersionUID = -6550920316795329101L;
	
	private final UUID id;

	public CiclistaNaoPossuiCadastroAtivoException(UUID id) {
		super("O ciclista informado não possui cadastro ativo");
		this.id = id;
	}

}
