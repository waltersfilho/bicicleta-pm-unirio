package br.unirio.bicicleta.exception;

import java.util.UUID;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 5715991698185311649L;
	
	private final transient Object id;

	public ResourceNotFoundException(UUID id) {
		super("Não foi possível encontrar a entidade solicitada");
		this.id = id;
	}
	
	public ResourceNotFoundException(Long id) {
		super("Não foi possível encontrar a entidade solicitada");
		this.id = id;
	}

}
