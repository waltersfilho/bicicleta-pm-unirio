package br.unirio.bicicleta.model.funcionario;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Funcionario {
	
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private Long matricula;
	
	@NotEmpty
	private String senha;
	
	@NotEmpty
	private String email;
	
	@NotEmpty
	private String nome;
	
	@NotNull
	private Integer idade;
	
	@NotEmpty
	private String funcao;
	
	@NotEmpty
	private String cpf;
}
