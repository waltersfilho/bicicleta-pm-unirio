package br.unirio.bicicleta.model.dto;

import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CadastroCiclistaDTO {
	
	private Ciclista ciclista;
	private MeioDePagamento meioDePagamento;

}
