package br.unirio.bicicleta.model.dto;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RealizarCobrancaDTO {

	private Double valor;
	private UUID ciclistaId;
}
