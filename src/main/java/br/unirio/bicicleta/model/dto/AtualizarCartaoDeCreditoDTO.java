package br.unirio.bicicleta.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtualizarCartaoDeCreditoDTO {
	
	private String nomeTitular;
	private String numero;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date validade;
	
	private String cvv;
	
}
