package br.unirio.bicicleta.model.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BicicletaDTO {
	
	private UUID id;
	private String marca;
	private String modelo;
	private String ano;
	private Integer numero;
	private String status;

}
