package br.unirio.bicicleta.model.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrancaDTO {
	
	private UUID id;

    private Integer numero;

    private String localizacao;

    private String anoDeFabricacao;

    private String status;

    private UUID bicicleta;
    
    private String modelo;

}
