package br.unirio.bicicleta.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CadastroFuncionarioDTO {
	
	private String senha;
	private String email;
	private String nome;
	private Integer idade;
	private String funcao;
	private String cpf;

}
