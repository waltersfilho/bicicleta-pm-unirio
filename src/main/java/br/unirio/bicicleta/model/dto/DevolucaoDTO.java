package br.unirio.bicicleta.model.dto;

import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DevolucaoDTO {
	
	private UUID idTranca;
	private UUID idBicicleta;

}
