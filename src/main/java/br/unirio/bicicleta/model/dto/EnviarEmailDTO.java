package br.unirio.bicicleta.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EnviarEmailDTO {
	
	private String email;
	private String mensagem;

}
