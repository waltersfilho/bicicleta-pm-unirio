package br.unirio.bicicleta.model.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.unirio.bicicleta.model.ciclista.Passaporte;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NotEmpty
public class AtualizarCiclistaDTO {

	private String nome;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date nascimento;

	private String cpf;

	private Passaporte passaporte;
	private String nacionalidade;
	private String email;

}