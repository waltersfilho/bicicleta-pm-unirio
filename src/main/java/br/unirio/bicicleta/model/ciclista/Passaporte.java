package br.unirio.bicicleta.model.ciclista;

import java.util.Date;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class Passaporte {
	
	@NotEmpty
	private String numero;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull
	private Date validade;
	
	@NotEmpty
	private String pais;
	
}
