package br.unirio.bicicleta.model.ciclista;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Ciclista {
	
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private UUID id;
	
	@NotEmpty
	private String nome;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull
	private Date nascimento;
	
	@NotEmpty
	private String cpf;
	
	@NotNull
	private Passaporte passaporte;
	
	@NotEmpty
	private String nacionalidade;
	
	@NotEmpty
	@Email
	private String email;
	
	@NotEmpty
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;

	private String status = "Criado";
}
