package br.unirio.bicicleta.model.ciclista;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MeioDePagamento {
	
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private UUID id;
	
	@NotEmpty
	private String nomeTitular;
	
	@NotEmpty
	private String numero;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@NotNull
	private Date validade;
	
	@NotEmpty
	private String cvv;
	
	@OneToOne
	@JsonProperty(access = Access.WRITE_ONLY)
	private Ciclista ciclista;

}
