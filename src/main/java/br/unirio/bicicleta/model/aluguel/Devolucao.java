package br.unirio.bicicleta.model.aluguel;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Devolucao {
	
	@Id
	@GeneratedValue
	private UUID id;
	
	private UUID bicicleta;
	private Date horaInicio;
	private UUID trancaFim;
	private Date horaFim;
	private UUID cobranca;
	private UUID ciclista;
}
