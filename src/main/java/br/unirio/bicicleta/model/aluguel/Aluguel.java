package br.unirio.bicicleta.model.aluguel;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
@NoArgsConstructor
public class Aluguel {
	
	@Id
	@GeneratedValue
	private UUID id;
	
	private UUID bicicleta;
	private Date horaInicio;
	private UUID trancaFim;
	private Date horaFim;
	private UUID cobranca;
	private UUID ciclista;
	private UUID trancaInicio;

}
