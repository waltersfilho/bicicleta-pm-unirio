package br.unirio.bicicleta.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("app.aluguel")
@Getter
@Setter
public class CustomProperties {

    private String hostEquipamento = "http://localhost:8080";
    private String hostCobranca = "http://localhost:8082";

}
