package br.unirio.bicicleta.controller.funcionario;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.unirio.bicicleta.model.dto.AtualizarFuncionarioDTO;
import br.unirio.bicicleta.model.dto.CadastroFuncionarioDTO;
import br.unirio.bicicleta.model.funcionario.Funcionario;
import br.unirio.bicicleta.service.IFuncionarioService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/funcionario")
@AllArgsConstructor
public class FuncionarioController {

	private IFuncionarioService funcionarioService;
	

	@GetMapping
	public List<Funcionario> recuperarFuncionarios() {

		return funcionarioService.recuperarFuncionarios();
	}

	@PostMapping
	public Funcionario cadastrarFuncionario(@Valid @RequestBody CadastroFuncionarioDTO cadastroFuncionarioDTO) {

		return funcionarioService.inserirFuncionario(cadastroFuncionarioDTO);
	}

	@PutMapping("/{idFuncionario}")
	public Funcionario atualizarFuncionario(@Valid @RequestBody AtualizarFuncionarioDTO atualizarFuncionarioDTO,
			@PathVariable(value = "idFuncionario") Long idFuncionario) {

		return funcionarioService.atualizarFuncionario(atualizarFuncionarioDTO, idFuncionario);
	}

	@DeleteMapping("/{idFuncionario}")
	public void deletarFuncionario(@PathVariable(value = "idFuncionario") Long idFuncionario) {

		funcionarioService.deletarFuncionario(idFuncionario);
	}

	@GetMapping("/{idFuncionario}")
	public Funcionario recuperarFuncionario(@PathVariable(value = "idFuncionario") Long idFuncionario) {
		
		return funcionarioService.buscarPorID(idFuncionario);

	}

}
