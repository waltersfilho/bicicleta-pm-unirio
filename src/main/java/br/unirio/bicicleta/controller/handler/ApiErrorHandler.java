package br.unirio.bicicleta.controller.handler;

import java.util.UUID;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.error.ErrorResponse;
import br.unirio.bicicleta.exception.AluguelEmAndamentoException;
import br.unirio.bicicleta.exception.BicicletaNaoDisponivelException;
import br.unirio.bicicleta.exception.BicicletaNaoHabilitadaException;
import br.unirio.bicicleta.exception.CPFObrigatorioException;
import br.unirio.bicicleta.exception.CiclistaNaoPossuiCadastroAtivoException;
import br.unirio.bicicleta.exception.EmailExistenteException;
import br.unirio.bicicleta.exception.MetodoDePagamentoInvalidoException;
import br.unirio.bicicleta.exception.PassaporteInvalidoException;
import br.unirio.bicicleta.exception.ResourceNotFoundException;
import br.unirio.bicicleta.exception.TrancaVaziaException;

@RestControllerAdvice
public class ApiErrorHandler {
	
	@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler({ConstraintViolationException.class, UnirestException.class, MetodoDePagamentoInvalidoException.class})
	public ErrorResponse handle(Exception exception) {
		
		return new ErrorResponse(UUID.randomUUID(), "BAD422", exception.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResourceNotFoundException.class)
	public ErrorResponse handle(ResourceNotFoundException resourceNotFoundException) {
		
		return new ErrorResponse(resourceNotFoundException.getId(), "BAD400", resourceNotFoundException.getMessage());
	}	
	
	@ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED)
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ErrorResponse handle(HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedException) {
		
		return new ErrorResponse(UUID.randomUUID(), "MET405", httpRequestMethodNotSupportedException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ErrorResponse handle(MethodArgumentTypeMismatchException methodArgumentTypeMismatchException) {
		
		return new ErrorResponse(UUID.randomUUID(), "BAD400", methodArgumentTypeMismatchException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.FORBIDDEN)
	@ExceptionHandler(AluguelEmAndamentoException.class)
	public ErrorResponse handle(AluguelEmAndamentoException aluguelEmAndamentoException) {
		
		return new ErrorResponse(aluguelEmAndamentoException.getId(), "BAD403", aluguelEmAndamentoException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BicicletaNaoDisponivelException.class)
	public ErrorResponse handle(BicicletaNaoDisponivelException bicicletaNaoDisponivelException) {
		
		return new ErrorResponse(bicicletaNaoDisponivelException.getId(), "BIC401", bicicletaNaoDisponivelException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.FORBIDDEN)
	@ExceptionHandler(CiclistaNaoPossuiCadastroAtivoException.class)
	public ErrorResponse handle(CiclistaNaoPossuiCadastroAtivoException ciclistaNaoPossuiCadastroAtivoException) {
		
		return new ErrorResponse(ciclistaNaoPossuiCadastroAtivoException.getId(), "CIC401", ciclistaNaoPossuiCadastroAtivoException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TrancaVaziaException.class)
	public ErrorResponse handle(TrancaVaziaException trancaVaziaException) {
		return new ErrorResponse(trancaVaziaException.getId(), "TRN400", trancaVaziaException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.FORBIDDEN)
    @ExceptionHandler(BicicletaNaoHabilitadaException.class)
    public ErrorResponse handle(BicicletaNaoHabilitadaException bicicletaNaoHabilitadaException) {

        return new ErrorResponse(bicicletaNaoHabilitadaException.getId(), "BNH001", bicicletaNaoHabilitadaException.getMessage());
    }
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(EmailExistenteException.class)
	public ErrorResponse handle(EmailExistenteException emailExistenteException) {

	    return new ErrorResponse(emailExistenteException.getEmail(), "EME001", emailExistenteException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(CPFObrigatorioException.class)
	public ErrorResponse handle(CPFObrigatorioException cpfObrigatorioException) {

	    return new ErrorResponse(UUID.randomUUID(), "CPF001", cpfObrigatorioException.getMessage());
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PassaporteInvalidoException.class)
	public ErrorResponse handle(PassaporteInvalidoException passaporteInvalidoException) {

	    return new ErrorResponse(UUID.randomUUID(), "PBI001", passaporteInvalidoException.getMessage());
	}
}
