package br.unirio.bicicleta.controller.meiodepagamento;


import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.ciclista.MeioDePagamento;
import br.unirio.bicicleta.model.dto.AtualizarCartaoDeCreditoDTO;
import br.unirio.bicicleta.service.IMeioDePagamentoService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/cartaoDeCredito")
@AllArgsConstructor
public class MeioDePagamentoController {
	
	private IMeioDePagamentoService meioDePagamentoService;

	@GetMapping("/{idCiclista}")
	public MeioDePagamento recuperarCartaoDeCredito(@PathVariable(value = "idCiclista") UUID idCiclista) {
		
		return meioDePagamentoService.recuperarMeioDePagamento(idCiclista);
	}

	@PutMapping("/{idCiclista}")
	public MeioDePagamento atualizarCartaoDeCredito(
			@RequestBody AtualizarCartaoDeCreditoDTO atualizarCartaoDeCreditoDTO, @PathVariable(value = "idCiclista") UUID idCiclista)
					throws JsonProcessingException, UnirestException {
	
		return meioDePagamentoService.atualizarMeioDePagamento(atualizarCartaoDeCreditoDTO, idCiclista);
	}

}
