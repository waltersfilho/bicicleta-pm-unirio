package br.unirio.bicicleta.controller.aluguel;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.aluguel.Aluguel;
import br.unirio.bicicleta.model.aluguel.Devolucao;
import br.unirio.bicicleta.model.dto.AluguelDTO;
import br.unirio.bicicleta.model.dto.DevolucaoDTO;
import br.unirio.bicicleta.service.IAluguelService;

@RestController
public class AluguelController {
	
	private IAluguelService aluguelService;
	
	public AluguelController(IAluguelService aluguelService) {
		this.aluguelService = aluguelService;
	}

	@PostMapping("aluguel")
	public Aluguel realizarAluguel(@RequestBody AluguelDTO aluguelDTO) throws UnirestException, JsonProcessingException {
		
		return aluguelService.realizarAluguel(aluguelDTO);
	}

	@PostMapping("devolucao")
	public Devolucao realizarDevolucao(@RequestBody DevolucaoDTO devolucaoDTO) throws JsonProcessingException, UnirestException {

		return aluguelService.realizarDevolucao(devolucaoDTO);
	}

}
