package br.unirio.bicicleta.controller.ciclista;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.exceptions.UnirestException;

import br.unirio.bicicleta.model.ciclista.Ciclista;
import br.unirio.bicicleta.model.dto.AtualizarCiclistaDTO;
import br.unirio.bicicleta.model.dto.CadastroCiclistaDTO;
import br.unirio.bicicleta.service.external.ICiclistaService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/ciclista")
@AllArgsConstructor
public class CiclistaController {

	private ICiclistaService ciclistaService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Ciclista cadastrarCiclista(@Valid @RequestBody CadastroCiclistaDTO cadastroCiclistaDTO) throws UnirestException, JsonProcessingException {

		return ciclistaService.inserirCiclista(cadastroCiclistaDTO);
	}

	@GetMapping("/{idCiclista}")
	public Ciclista recuperarCiclista(@PathVariable(value = "idCiclista") UUID idCiclista) {
		
		return ciclistaService.buscarPorID(idCiclista);
	}

	@PutMapping("/{idCiclista}")
	public Ciclista atualizarCiclista(@RequestBody AtualizarCiclistaDTO atualizarCiclistaDTO,
			@PathVariable(value = "idCiclista") UUID idCiclista) {

		return ciclistaService.atualizarCiclista(atualizarCiclistaDTO, idCiclista);
	}

	@PostMapping("/{idCiclista}/ativar")
	public Ciclista ativarCiclista(@PathVariable(value = "idCiclista") UUID idCiclista) {

		return ciclistaService.ativarCiclista(idCiclista);
	}

	@GetMapping("/existeEmail/{email}")
	public Boolean existeEmail(@PathVariable(value = "email", required = true) String email) {

		return ciclistaService.existeEmail(email);
	}

}
